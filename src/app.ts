import express from "express";

const app = express();

app.use(express.json());

const ToDoList = [
    { TaskID: 1, TaskName: "Task 1", TaskStatus: "Pending", },
    { TaskID: 2, TaskName: "Task 2", TaskStatus: "Pending", },
    { TaskID: 3, TaskName: "Task 3", TaskStatus: "Pending", },
]

// Get List of Tasks
app.get('/GET/getTasks', (req, res) => {
    return res.send(ToDoList);
});

// Add New Task
app.post('/POST/newTask', (req, res) => {
    const TaskList = {
        TaskID: ToDoList.length + 1,
        TaskName: req.body.TaskName,
        TaskStatus: "Pending"
    }
    ToDoList.push(TaskList);
    return res.send(ToDoList)
});

// Update Specific Task
app.put('/PUT/updateTask', (req, res) => {
    let i: any;
    i = ToDoList.find(x => x.TaskID == parseInt(req.body.TaskID));
    i.TaskName = req.body.TaskName;
    i.TaskStatus = req.body.TaskStatus;
    return res.send(ToDoList);
});

// Delete Specific Task
app.delete('/DELETE/removeTask', (req, res) => {
    let i: any;
    i = ToDoList.find(x => x.TaskID == parseInt(req.body.TaskID));
    const cursor = ToDoList.indexOf(i);
    ToDoList.splice(cursor, 1);
    res.send(ToDoList);
});

app.listen(3000, () => console.log('Server running.'));